package com.epam;

import com.epam.fibonacciNumbers.FibonacciNumbers;
import com.epam.simpleNumbers.SimpleNumbers;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        System.out.println("Please, choose the program:\n" +
                "1 - Simple numbers\n" +
                "2 - Fibonacci numbers\n" +
                "0 - exit\n" +
                "make the choice: "
        );

        Scanner scanner = new Scanner(System.in);
        String menu = scanner.nextLine();
        while (!menu.equals("0")){
            if (menu.equals("1")){
                new SimpleNumbers().start();
            }else if (menu.equals("2")){
                new FibonacciNumbers().start();
            }else {
                System.out.println("You entered the wrong value!");
            }
            System.out.println("\n Make new choice: \n" +
                    "1 - Simple numbers\n" +
                    "2 - Fibonacci numbers\n" +
                    "0 - exit\n" +
                    "make the choice: "
            );
            menu = scanner.nextLine();
        }


    }
}

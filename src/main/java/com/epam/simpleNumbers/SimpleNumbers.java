package com.epam.simpleNumbers;

import java.util.Arrays;
import java.util.Scanner;

public class SimpleNumbers {
    private int[] numbers = new int[1];

    public void start() {
        System.out.println("\n SimpleNumbers started!");
        createNumbers();
        printMenu();
        Scanner scanner = new Scanner(System.in);
        String menu = scanner.nextLine();
        while (!menu.equals("0")) {
            switch (menu) {
                case "1":
                    printOdd();
                    break;
                case "2":
                    printEven();
                    break;
                case "3":
                    printSumOdd();
                    break;
                case "4":
                    printSumEven();
                    break;
                default:
                    System.out.println("You entered the wrong value!");
            }
            printMenu();
            menu = scanner.nextLine();
        }
    }

    private void createNumbers() {
        System.out.println("Enter the interval (for example: [1;100], without []):");
        boolean isException = true;
        while (isException) {
            try {
                String interval = new Scanner(System.in).nextLine();
                String[] split = interval.split(";");
                int first = new Integer(split[0]);
                int last = new Integer(split[1]);
                numbers[0] = first;
                for (int i = first + 1; i <= last; i++) {
                    int[] newArr = Arrays.copyOf(numbers, numbers.length + 1);
                    newArr[newArr.length - 1] = i;
                    numbers = newArr;
                }
                isException = false;
            } catch (Exception e) {
                System.out.println("\n You entered the wrong value!");
                System.out.println("Enter the interval (for example: [1;100], without []):");
            }

        }
        System.out.println(Arrays.toString(numbers));
    }

    private void printMenu() {
        System.out.println("\n Make next choice:\n" +
                "1 - print odd numbers from start to end of interval,\n" +
                "2 - print even numbers from end to start of interval,\n" +
                "3 - print the sum of odd numbers,\n" +
                "4 - print the sum of even numbers,\n" +
                "0 - exit.\n" +
                "make the choice: "
        );
    }

    private void printOdd() {
        System.out.println("\n Odd numbers from start to end:");
        System.out.print("[" + " ");
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 != 0) {
                System.out.print(numbers[i] + " ");
            }
        }
        System.out.println("]");
    }

    private void printEven() {
        System.out.println("\n Even numbers from end to start:");
        System.out.print("[" + " ");
        for (int i = numbers.length - 1; i > -1; i--) {
            if (numbers[i] % 2 == 0) {
                System.out.print(numbers[i] + " ");
            }
        }
        System.out.println("]");
    }

    private void printSumOdd() {
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 != 0) {
                sum += numbers[i];
            }
        }
        System.out.println("The sum of odd numbers: " + sum);
    }

    private void printSumEven() {
        int sum = 0;
        for (int i = numbers.length - 1; i > -1; i--) {
            if (numbers[i] % 2 == 0) {
                sum += numbers[i];
            }
        }
        System.out.println("\n The sum of even numbers: " + sum);
    }
}

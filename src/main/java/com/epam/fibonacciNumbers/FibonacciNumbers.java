package com.epam.fibonacciNumbers;

import java.util.Arrays;
import java.util.Scanner;

public class FibonacciNumbers {
    private int[] fiboNumbers = {1, 1};

    public void start() {
        System.out.println("FibonacciNumbers started!");
        printBuildMenu();
        Scanner scanner = new Scanner(System.in);
        String buildMenu = scanner.nextLine();
        while (!buildMenu.equals("0")) {
            switch (buildMenu) {
                case "1":
                    buildWithTheBiggestOddNumber();
                    printFiboNumbers();
                    printPercentage();
                    break;
                case "2":
                    buildWithTheBiggestEvenNumber();
                    printFiboNumbers();
                    printPercentage();
                    break;
                default:
                    System.out.println("You entered the wrong value!");
            }
            printBuildMenu();
            buildMenu = scanner.nextLine();
        }
    }

    private void printBuildMenu() {
        System.out.println("\n Choose the method of build:\n" +
                "1 - the biggest number well be odd,\n" +
                "2 - the biggest number well be even,\n" +
                "0 - exit.\n" +
                "make the choice: "
        );
    }

    private void buildWithTheBiggestOddNumber() {
        fiboNumbers = new int[]{1, 1};
        boolean isException = true;
        while (isException) {
            try {
                System.out.println("\n Please, enter max size of set (the biggest number will be odd):");
                int size = new Integer(new Scanner(System.in).nextLine());
                if (size == 2) {
                    isException = false;
                } else if (size > 2) {
                    for (int i = 3; i <= size; i++) {
                        buildArrWithMaxSize();
                        isException = false;
                    }
                    if (fiboNumbers[fiboNumbers.length - 1] % 2 == 0) {
                        fiboNumbers = Arrays.copyOf(fiboNumbers, fiboNumbers.length - 1);
                        isException = false;
                    }
                } else {
                    System.out.println("You entered the wrong value!");
                }
            } catch (Exception e) {
                System.out.println("You entered the wrong value!");
            }
        }
    }

    private void buildWithTheBiggestEvenNumber() {
        fiboNumbers = new int[]{1, 1};
        boolean isException = true;
        while (isException) {
            try {
                System.out.println("\n Please, enter max size of set (the biggest number will be even):");
                int size = new Integer(new Scanner(System.in).nextLine());
                if (size > 2) {
                    for (int i = 3; i <= size; i++) {
                        buildArrWithMaxSize();
                        isException = false;
                    }
                    if (fiboNumbers[fiboNumbers.length - 1] % 2 != 0) {
                        fiboNumbers = Arrays.copyOf(fiboNumbers, fiboNumbers.length - 1);
                        if (fiboNumbers[fiboNumbers.length - 1] % 2 != 0) {
                            fiboNumbers = Arrays.copyOf(fiboNumbers, fiboNumbers.length - 1);
                        }
                        isException = false;
                    }

                } else {
                    System.out.println("You entered the wrong value!");
                }
            } catch (Exception e) {
                System.out.println("You entered the wrong value!");
            }
        }
    }

    private void buildArrWithMaxSize() {
        int[] newArr = Arrays.copyOf(fiboNumbers, fiboNumbers.length + 1);
        newArr[newArr.length - 1] = newArr[newArr.length - 3] + newArr[newArr.length - 2];
        fiboNumbers = newArr;
    }


    private void printFiboNumbers() {
        System.out.println(Arrays.toString(fiboNumbers));
    }

    private void printPercentage() {
        int countOdd = 0;
        int countEven = 0;
        for (int i = 0; i < fiboNumbers.length; i++) {
            if (fiboNumbers[i] % 2 != 0) {
                countOdd++;
            } else {
                countEven++;
            }
        }
        int percentOdd = (int) Math.round((double) countOdd*100/(countOdd + countEven));
        int percentEven = (int) Math.round((double) countEven*100/(countOdd + countEven));
        System.out.println("Percentage of odd and even Fibonacci numbers: " + percentOdd + "/" + percentEven);
    }
}
